<?php

namespace Pongsit\Firebase\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Pongsit\User\Models\User;
use Pongsit\Profile\Models\Profile;
use Firebase\JWT\JWT;
Use Firebase\JWT\Key;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Throwable;

class FirebaseController extends Controller
{
	public function newUser($infos){
		$user = new User;
		$user->email = $infos['email'];
		$user->name = $infos['name'];
		$user->password = bcrypt($infos['password']);
		$user->slug = slug($user,'name');
		$user->save();
		return $user;
	}

	public function loginPhone(Request $request){
		$request->validate([
			'idToken' => 'required',
		],[
			'idToken.required'=>'ไม่พบ accessToken กรุณาติดต่อ Admin',
		]);

		$pkeys_raw = file_get_contents("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com");
		$keys = json_decode($pkeys_raw, true);

		foreach($keys as $v){
			$v = str_replace('""','',$v);
			try{
				$decoded = JWT::decode($request->idToken, new Key($v, 'RS256'));
			}catch (\Exception $e) {
			
			}
			
			if(!empty($decoded)){
				break;
			}
		}

		// try{
		// 	$decoded = JWT::decode($request->idToken, new Key($publicKey, 'RS256'));
		// }catch (\Exception $e) {
		// 	dd($e);
		// 	return redirect()->route('firebase.logout')->with(['error'=>'เกิดการผิดพลาดบางอย่าง กรุณาลองใหม่อีกครั้ง']);
		// }

		$variables['phone_number'] = str_replace('+66','0',$decoded->phone_number);
		$variables['user_id'] = $decoded->user_id;

		$user = User::where('firebase_id', $variables['user_id'])->first();
		if(empty($user)){
			if(empty($variables['email'])){
				if(!empty(env('SITE_DOMAIN'))){
					$variables['email'] = $variables['phone_number'].'@'.env('SITE_DOMAIN');
				}else{
					dd('Please set the domain in env.');
				}
			}

			try{
				$newUser = $this->newUser([
					'name' => $variables['phone_number'],
					'email' => $variables['email'],
					'password' => Str::random(10),
				]);
				$newUser['firebase_id'] = $variables['user_id'];
				$newUser->save();
				$user = $newUser;
				$user->profile()->save(new Profile([
					'phone' => $variables['phone_number'],
					'phone_verified_at' => vela()->now()
				]));
			}catch(\Throwable $e){
				// dd($e);
				// $errorCode = $e->errorInfo[1];
				// if($errorCode == 1062){
					return redirect('/')->with(['error'=>'มีอีเมลนี้อยู่ในระบบแล้ว หากยังไม่สามารถเข้าระบบได้กรุณาติดต่อ Admin ครับ']);
					// dd('Duplicate! If you own the account, please sign in.');
					// duplicate entry problem
					// return redirect('/message/duplicate');
				// }
			}
		}

		Auth::loginUsingId($user->id, true);
		session(['user' => $user]);
		return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
	}

	public function logout(Request $request){
		Auth::logout();
		$request->session()->flush();

		// return redirect('/')->with(['success'=>'ออกจากระบบเรียบร้อย']);
		return view('firebase::logout');
	}

}
