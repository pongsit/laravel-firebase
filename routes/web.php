<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Firebase\Http\Controllers\FirebaseController;

// Firebase
Route::post('/login/phone', [FirebaseController::class, 'loginPhone'])->name('firebase.login.phone');
Route::get('/logout', [FirebaseController::class, 'logout'])->name('firebase.logout');