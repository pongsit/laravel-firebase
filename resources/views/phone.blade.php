<!DOCTYPE html>
<html lang="en">
<head>
    <x:head />
    <style>
        body{
            height: 100vh;
            color: rgb(148 163 184);
            background-color: #0F1A29;
            background-image: radial-gradient(145.05% 100% at 50% 0%,#1D2B41 0%,#020509 57.38%,#0F1A29 88.16%);
        }
    </style>
</head>
<body>
    <x:header />
    <div class="container" >
        <div class="row justify-content-center mb-0 pb-0 pt-3">
            <div class="col-12 pb-0 mb-0 text-center d-flex justify-content-center">
                <div class="col-3 col-lg-2 text-end d-flex align-items-center">
                    <img class="w-100" src="{{asset('img/section1.png')}}">
                </div>
                <div class="px-3" style="font-size: 36px;">
                    สมัครใหม่
                </div>
                <div class="col-3 col-lg-2 text-start d-flex align-items-center">
                    <img class="w-100"  src="{{asset('img/section2.png')}}">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 p-0 text-center d-flex justify-content-center">
                <div class="px-3 text-white" style="font-size: 24px;">
                    <em>ชั้นตอนการเข้าสู่ระบบหรือสมัครสมาชิก</em>
                </div>
            </div>
        </div>
        <div id="loading" class="text-center" style="display: none;">
            <i style="font-size: 90px; position:relative; top:30px;" class="fas fa-phone-square-alt"></i> 
            <img src="{{asset('vendor/pongsit/system/img/loading/light.gif')}}"><br>
            กรุณารอสักครู่กำลังดำเนินการครับ
        </div>
        <div id="sign-in-form">
            <div class="row justify-content-center">
                <div class="col-10 col-sm-8 col-md-6 col-lg-5 col-xl-4 p-0 text-center d-flex justify-content-center">
                    <div class="input-group">
                        <span class="input-group-text px-3" id="basic-addon1"><i style="font-size: 20px;" class="fas fa-phone"></i></span>
                        <input id="phone-number" class="form-control me-2" style="background-color: #1D2B41; color:white;" type="text" placeholder="เบอร์โทรศัพท์">
                    </div>
                    <button id="sign-in-button" class="btn border text-nowrap rounded" style="bottom: 0;background-color: #1D2B41; color:white;">ส่งข้อมูล</button>
                    <button id="sign-in-button2" class="d-none" style="bottom: 0;background-color: #1D2B41; color:white;">ส่งข้อมูล</button>
                </div>
            </div>
        </div>
        <form id="verification-code-form" action="#" style="display:none;">
            <div class="row justify-content-center">
                <div class="col-10 col-sm-8 col-md-6 col-lg-5 col-xl-4 p-0 text-center d-flex justify-content-center">
                    <input id="verification-code" class="form-control me-2" style="background-color: #1D2B41; color:white;" placeholder="กรุณากรอกรหัสที่ได้รับจากข้อความ" type="text" >
                    <input type="submit" id="verify-code-button" 
                        class="btn border text-nowrap" style="bottom: 0;background-color: #1D2B41; color:white;" value="ยืนยันรหัส">
                </div>
            </div>
        </form>
        <div class="row justify-content-center">
            <div class="col-12 p-0 text-center d-flex justify-content-center">
                <div class="px-3 d-flex" style="font-size: 24px;">
                    <div id="step1" class="d-sm-flex"><div class="me-2">1. </div>กรอกเบอร์</div>
                    <div class="mx-3 d-flex align-items-center" style="">
                        <i class="fas fa-circle" style="font-size: 5px;"></i>
                    </div>
                    <div id="step2" class="d-sm-flex"><div class="me-2">2. </div>ยืนยัน OTP</div>
                    <div class="mx-3 d-flex align-items-center" style="">
                        <i class="fas fa-circle" style="font-size: 5px;"></i>
                    </div>
                    <div id="step3" class="d-sm-flex"><div class="me-2">3. </div>เสร็จสิ้น</div>
                </div>
            </div>
        </div>
        <form id="login-phone" method="post" action="{{route('firebase.login.phone')}}" style="display:none;">
            @csrf
            <input type="hidden" id="idToken" name="idToken">
        </form>
    </div>
    <x:footer />
    <script src="{{asset('js/firebase-auth_phone_signin.js')}}"></script>
</body>
</html>