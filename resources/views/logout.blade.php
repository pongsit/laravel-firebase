<!DOCTYPE html>
<html lang="en">
<head>
    <x:head />
</head>
<body>
    <x:header />
    <div class="container" >
        <x:section.type1 title="ออกจากระบบ" />
        <div id="loading" class="text-center">
            กรุณารอสักครู่กำลังดำเนินการครับ
        </div>
        <script>
            var logoutSuccess = function(){
                window.location = "{{route('logout.success')}}";
            }
            var logoutError = function(){
                window.location = "{{route('logout.error')}}";
            }
        </script>
        <script src="{{asset('js/firebase-auth_sign_out.js')}}"></script>
    </div>
    <x:footer />
</body>
</html>