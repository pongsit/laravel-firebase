import { getAuth, signOut } from "firebase/auth";

window.auth = getAuth(window.app);

window.onload = function() {
  signOut(window.auth).then(() => {
    logoutSuccess();
  }).catch((error) => {
    logoutError();
  });
}