import { getAuth, RecaptchaVerifier, signInWithPhoneNumber, onAuthStateChanged } from "firebase/auth";

window.auth = getAuth(window.app);

window.onload = function() {
  window.recaptchaVerifier = new RecaptchaVerifier('sign-in-button2', {
    'size': 'invisible',
    'callback': (response) => {
      // reCAPTCHA solved, allow signInWithPhoneNumber.
      onSignInSubmit();
      // setTimeout(function(){ window.location = window.location.href; }, 60000);
    }
  }, window.auth);
  recaptchaVerifier.render().then((widgetId) => {
    window.recaptchaWidgetId = widgetId;
  });
  onAuthStateChanged(window.auth, (user) => {
    if (user) {
      // User is signed in, see docs for a list of available properties
      // https://firebase.google.com/docs/reference/js/firebase.User
      const uid = user.uid;
      getToken();
      // ...
    } else {
      // User is signed out
      // ...
    }
  });
}

$(function(){

  $('#phone-number').focus();
  $('#step1').addClass('this_step');

  $('body').on('click tap','#sign-in-button',function(){
    $('#sign-in-form').hide();
    $('#loading').show();
    $('#sign-in-button2').click();
  });

  $(document).on('keypress','#sign-in-form',function(e) {
    if(e.which == 13) {
      $('#sign-in-button').click();
    }
  });

  $('body').on('click tap','#verify-code-button',function(){
    $('#verification-code-form').hide();
    $('#loading').show();
    var code = $('#verification-code').val();
    if (code) {
        confirmationResult.confirm(code).then(function (result) {
            // User signed in successfully.
            var user = result.user;
        }).catch(function (error) {
            console.log(error);
            Swal.fire({
              icon: 'error',
              title: 'ขออภัย',
              text: "รหัสไม่ถูกต้องครับ",
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'OK'
            }).then((result) => {
              $('#loading').hide();
              $('#verification-code-form').show();
            });
        });
    }
  });

  $(document).on('keypress','#verification-code-form',function(e) {
    if(e.which == 13) {
      $('#verify-code-button').click();
    }
  });
});

function onSignInSubmit() {
  // if (isPhoneNumberValid()) {
    var phoneNumber = $('#phone-number').val();
    phoneNumber = '+66'+phoneNumber.substr(1);
    var appVerifier = window.recaptchaVerifier;
    console.log('data');
    signInWithPhoneNumber(auth, phoneNumber, appVerifier)
      .then((confirmationResult) => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        window.confirmationResult = confirmationResult;
        $('#sign-in-form').hide();
        $('#step1').removeClass('this_step');
        $('#step2').addClass('this_step');
        $('#verification-code-form').show();
        $('#verification-code').focus();
        $('#loading').hide();
      }).catch((error) => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'ขออภัย',
          text: "กรุณารอสักครู่แล้วลองใหม่อีกครั้งครับ",
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK'
        }).then((result) => {
          window.location = window.location.href;
        });
      });
  // }else{
  //   Swal.fire({
  //       icon: 'error',
  //       title: 'ขออภัย',
  //       text: 'กรุณากรอกตัวเลขเท่านั้น'
  //   });
  //   $('#loading').hide();
  //   $('#sign-in-form').show();
  //   $('#verification-code-form').hide();
  //   $('#phone-number').focus();
  // }
  
}

function isPhoneNumberValid() {
  var pattern = /^0\d{9}$/;
  var phoneNumber = $('#phone-number').val();
  return phoneNumber.search(pattern) !== -1;
}

function getToken() {
  var user = window.auth.currentUser;
  if (user) {
    window.auth.currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
      $('#idToken').val(idToken);
      $('#login-phone').submit();
    }).catch(function(error) {
      // Handle error
    });
  } else {
    // document.getElementById('sign-in-status').textContent = 'Signed out';
    // document.getElementById('account-details').textContent = 'null';
  }
}

