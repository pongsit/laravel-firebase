import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";

window.app = initializeApp(window.firebaseConfig);
window.auth = getAuth(window.app);
window.analytics = getAnalytics(window.app);