## install
- php artisan vendor:publish
- add js to webpack.mix.js
- npm install
- npm run watch

## update firebase version
- change firebase version

"dependencies": {
    "firebase": "^9.18.0"
}

in package.json
- npm install
- npm run watch

## create new project on firebase console
- Authentication > sign in method > add phone provider
- Authentication > Settings > Authorized domains 